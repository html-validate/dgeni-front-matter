# dgeni-front-matter changelog

## 5.0.0 (2024-04-05)

### ⚠ BREAKING CHANGES

- require nodejs 20

### Features

- require nodejs 20 ([236342c](https://gitlab.com/html-validate/dgeni-front-matter/commit/236342c53ca6eda1c837b1c3a2d732ac71d90d67))

## [4.1.0](https://gitlab.com/html-validate/dgeni-front-matter/compare/v4.0.0...v4.1.0) (2023-07-30)

### Features

- **deps:** require nodejs 16 or later ([b8ec968](https://gitlab.com/html-validate/dgeni-front-matter/commit/b8ec96824d5e73617aad80eb2a226dce85006be7))

## [4.0.0](https://gitlab.com/html-validate/dgeni-front-matter/compare/v3.0.0...v4.0.0) (2022-05-08)

### ⚠ BREAKING CHANGES

- require node 14

### Features

- require node 14 ([eb63dd7](https://gitlab.com/html-validate/dgeni-front-matter/commit/eb63dd7e2d20cfe3924cab71381b53f6709b27a8))

## [3.0.0](https://gitlab.com/html-validate/dgeni-front-matter/compare/v2.0.3...v3.0.0) (2021-06-20)

### ⚠ BREAKING CHANGES

- require NodeJS 12

### Features

- require NodeJS 12 ([e091182](https://gitlab.com/html-validate/dgeni-front-matter/commit/e091182084bfdff50dfc4f7cb44ac10c97898a90))

### [2.0.3](https://gitlab.com/html-validate/dgeni-front-matter/compare/v2.0.2...v2.0.3) (2020-11-08)

### Bug Fixes

- move built files to `dist` folder ([4139b76](https://gitlab.com/html-validate/dgeni-front-matter/commit/4139b762a733856829c990f5fc500c3b92ef89db))

## [2.0.2](https://gitlab.com/html-validate/dgeni-front-matter/compare/v2.0.1...v2.0.2) (2020-11-01)

## [2.0.1](https://gitlab.com/html-validate/dgeni-front-matter/compare/v2.0.0...v2.0.1) (2020-10-25)

# [2.0.0](https://gitlab.com/html-validate/dgeni-front-matter/compare/v1.0.2...v2.0.0) (2020-05-23)

### Bug Fixes

- depend on base package to so it doesnt get dependant on import order ([568cda1](https://gitlab.com/html-validate/dgeni-front-matter/commit/568cda139e9f96d275d88d201d5af9a95e0fbc9a))
- dont depend on dgeni-packages/links ([20284bd](https://gitlab.com/html-validate/dgeni-front-matter/commit/20284bd7277853ed15726b790f13bb1806e55790))
- dont include spec-files in package ([d55d257](https://gitlab.com/html-validate/dgeni-front-matter/commit/d55d2575f2d964c2349e768628cc3710840f3646))

### BREAKING CHANGES

- the dependency on dgeni-packages/link has been removed and must be added by the user package itself if used.

## [1.0.2](https://gitlab.com/html-validate/dgeni-front-matter/compare/v1.0.1...v1.0.2) (2019-12-31)

### Bug Fixes

- export ([3afba6f](https://gitlab.com/html-validate/dgeni-front-matter/commit/3afba6f820ffbcc1682e790ceadef6982d2603b9))

## [1.0.1](https://gitlab.com/html-validate/dgeni-front-matter/compare/v1.0.0...v1.0.1) (2019-12-31)

### Bug Fixes

- build artifact missing files ([e71a9a2](https://gitlab.com/html-validate/dgeni-front-matter/commit/e71a9a26f48033bb48a56d7a9051d9c23c069970))

# 1.0.0 (2019-12-27)

### Features

- initial version ([e552c70](https://gitlab.com/html-validate/dgeni-front-matter/commit/e552c7087478a0040de44eec81c91065ba19658d))
