# Dgeni Front Matter

Process YAML Front Matter blocks from Markdown files.

## Usage

```js
const packagePath = __dirname;

/* create dgeni application */
const pkg = new Package("my-package", [require("dgeni-front-matter")]);

/* configure dgeni to process all markdown files with frontmatter */
pkg.config(function (readFilesProcessor) {
  readFilesProcessor.sourceFiles = [
    {
      include: "**/*.md",
      basePath: ".",
      fileReader: "frontMatterFileReader",
    },
  ];
});
```

## Document properties

- `doc.docType` defaults to `front-matter` but can be set by adding `docType`
  front matter property.
- `doctype.frontmatter` is always set to `true`.

All properties from front matter are assigned to `doc`.

Given a markdown file such as:

```markdown
---
foo: bar
---

lorem ipsum
```

The `doc` object would be:

```js
{
  docType: 'front-matter',
  frontMatter: true,
  content: 'lorem ipsum',
  foo: "bar"
}
```

## Templates

Templates for rendering documents are searched as following:

- `${ doc.template }`
- `${ doc.id }.${ doc.docType }.template.html`
- `${ doc.id }.template.html`
- `${ doc.docType }.template.html`
