import path from "canonical-path";
import type { FileReader, FileInfo } from "../types";
import frontMatterFileReaderFactory from "./front-matter";

describe("frontMatterFileReader", () => {
	let fileReader: FileReader;

	const createFileInfo = function (file: string, content: string, basePath: string): FileInfo {
		return {
			fileReader: fileReader.name,
			filePath: file,
			baseName: path.basename(file, path.extname(file)),
			extension: path.extname(file).replace(/^\./, ""),
			basePath,
			relativePath: path.relative(basePath, file),
			content,
		};
	};

	beforeEach(() => {
		fileReader = frontMatterFileReaderFactory();
	});

	describe("defaultPattern", () => {
		it("should match .md files", () => {
			expect.assertions(2);
			expect(fileReader.defaultPattern.test("abc.md")).toBeTruthy();
			expect(fileReader.defaultPattern.test("abc.js")).toBeFalsy();
		});
	});

	describe("getDocs", () => {
		it("should return an object containing info about the file and its contents", () => {
			expect.assertions(1);
			const fileInfo = createFileInfo(
				"foo/bar.md",
				"---\nfoo: bar\nspam: ham\n---\nA load of content",
				"base/path",
			);
			expect(fileReader.getDocs(fileInfo)).toEqual([
				{
					docType: "front-matter",
					content: "A load of content",
					frontMatter: true,
					foo: "bar",
					spam: "ham",
					startingLine: 1,
				},
			]);
		});
		it("should handle markdown without front-matter attributes", () => {
			expect.assertions(1);
			const fileInfo = createFileInfo("foo/bar.md", "A load of content", "base/path");
			expect(fileReader.getDocs(fileInfo)).toEqual([
				{
					content: "A load of content",
					startingLine: 1,
				},
			]);
		});
	});
});
