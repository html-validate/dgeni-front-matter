import fm from "front-matter";
import type { FileReader, FileInfo } from "../types";

interface Attributes {
	docType?: string;
	title?: string;
	name?: string;
}

export default function frontMatterFileReader(): FileReader {
	return {
		name: "frontMatterFileReader",
		defaultPattern: /\.md$/,
		getDocs(fileInfo: FileInfo) {
			const content = fm<Attributes>(fileInfo.content);

			if (!content.frontmatter) {
				return [
					{
						content: fileInfo.content,
						startingLine: 1,
					},
				];
			}

			const doc = {
				docType: "front-matter",
				frontMatter: true,
				content: content.body,
				startingLine: 1,
				...content.attributes,
			};
			return [doc];
		},
	};
}
