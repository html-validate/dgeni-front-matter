import { Package } from "dgeni";
import type { FileReader, ReadFilesProcessor } from "./types";
import FrontMatterFactory from "./file-readers/front-matter";

interface TemplateFinder {
	templatePatterns: string[];
}

function frontMatterReaderConfig(
	readFilesProcessor: ReadFilesProcessor,
	frontMatterFileReader: FileReader,
): void {
	const { fileReaders = [] } = readFilesProcessor;
	readFilesProcessor.fileReaders = [frontMatterFileReader, ...fileReaders];
}

function frontMatterTemplateConfig(templateFinder: TemplateFinder): void {
	templateFinder.templatePatterns = [
		"${ doc.template }",
		"${ doc.id }.${ doc.docType }.template.html",
		"${ doc.id }.template.html",
		"${ doc.docType }.template.html",
	];
}

const pkg = new Package("front-matter", []);
pkg.factory(FrontMatterFactory);
pkg.config(frontMatterReaderConfig);
pkg.config(frontMatterTemplateConfig);

module.exports = pkg;
