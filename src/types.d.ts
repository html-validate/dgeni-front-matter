export interface FileInfo {
	fileReader: string;
	filePath: string;
	baseName: string;
	extension: string;
	basePath: string;
	relativePath: string;
	content: string;
}

export interface Document {
	docType?: string;
	content: string;
	startingLine: number;
}

export interface FileReader {
	name: string;
	defaultPattern: RegExp;
	getDocs(fileInfo: FileInfo): Document[];
}

export interface ReadFilesProcessor {
	fileReaders: FileReader[];
}
